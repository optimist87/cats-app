# SETUP:

npm install

npm run client:install

## RUN DEV: (запускается и сервак и фронт. Далее перейти на localhost:8080)

npm run dev

## PRODUCTION:

npm run client:build

## RUN PRODUCTION:

npm run start

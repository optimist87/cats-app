const { Router } = require("express")
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")
const config = require("config")
const { check, validationResult } = require("express-validator")
const User = require("../models/User")

const router = Router()

// register
// /api/auth/register
router.post(
  "/register",
  [
    check("email", "Некорректный email").isEmail(),
    check("password", "Минимальная длина пароля 6 символов")
      .isLength({ min: 6 })
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          success: false,
          message: "Некорректные данные"
        })
      }

      const { email, password } = req.body
      const candidate = await User.findOne({ email })
      if (candidate) {
        return res.status(400).json({
          success: false,
          message: "Такой пользователь уже существует"
        })
      }
      const hashedPassword = await bcrypt.hash(password, 12)
      const user = new User({email, password: hashedPassword})
      await user.save()

      res.status(200).json({
        success: true,
        message: "Пользователь создан!"
      })

    } catch (e) {
      console.log(e.message)
      res.status(500).json({
        success: false,
        message: "Что-то пошло не так"
      })
    }
  }
)

// login
// /api/auth/login
router.post(
  "/login",
  [
    check("email", "Введите корректный email").normalizeEmail().isEmail(),
    check("password", "Введите пароль").exists()
  ],
  async (req, res) => {
    try {

      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          success: false,
          message: "Некорректные данные"
        })
      }

      const { email, password } = req.body

      const user = await User.findOne({ email })

      if (!user) {
        return res.status(400).json({ success: false, message: "Юзер не найден" })
      }

      const isMatch = await bcrypt.compare(password, user.password)

      if (!isMatch) {
        return res.status(400).json({
          success: false,
          message: "Неверный пароль",
        })
      }

      const token = jwt.sign(
        { userId: user.id },
        config.get("jwtSecret"),
        { expiresIn: "12h" }
      )

      res.json({
        success: true,
        token,
        userId: user.id,
        message: "Вы успешно вошли!"
      })

    } catch (e) {
      res.status(500).json({
        success: false,
        message: "Что-то пошло не так"
      })
    }
  }
)

module.exports = router

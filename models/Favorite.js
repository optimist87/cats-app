const {Schema, model, Types} = require("mongoose")

const schema = new Schema({
  id: { type: String, required: true, unique: true },
  url: { type: String, required: true, unique: true },
  width: { type: Number, required: false, unique: false },
  height: {type: Number, required: false, unique: false },
  date: { type: Date, default: Date.now },
  owner: {type: Types.ObjectId, ref: "User"}
})

module.exports = model("Favorite", schema)

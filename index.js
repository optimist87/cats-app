const express = require("express");
const path = require('path')
const cors = require('cors')
const config = require("config");
const mongoose = require("mongoose");
const socketIO = require('socket.io')();
const authRoutes = require("./routes/auth.routes");
const favoritesRoutes = require("./routes/favorites.routes");

const app = express()
app.use(cors())
app.use(express.json({ extended: true }))

app.use( "/api/auth", authRoutes )
app.use( "/api/fav", favoritesRoutes )

const routes = [
  "/api",
  "/image",
  "/favorites",
  "/login",
  "/register",
]

if (process.env.NODE_ENV === "production") {
  const indexFile = path.resolve(__dirname, "client", "dist", "index.html")
  app.use("/", express.static(path.join(__dirname, "client", "dist")))
  routes.forEach(route => {
    app.get(route, (req, res) => {
      res.sendFile(indexFile)
    })
  })
}

const hostname = "localhost";
const PORT = config.get("port") || process.env.PORT

async function start() {
  try {
    await mongoose.connect(config.get("mongoUrl"), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    })
    let server
    if (process.env.NODE_ENV === "development") {
      server = app.listen(PORT, () => console.log(`======App DEV ready on port ${PORT}!!!!========`))
    } else {
      server = app.listen(PORT, hostname, () => console.log(`===App PRODUCTION ready on port ${PORT}===`))
    }

    const io = server && socketIO.listen(server)
    console.log('socket listening on port ', PORT)
    io && io.on('connection', client => {
      client.on('join', (userId) => {
        client.join(userId)
        console.log(`user ${userId} joined`)
      })
      client.on('add', (...data) => {
        client.broadcast.to(data[0].user).emit('add', data[0])
      })
      client.on('delete', (...data) => {
        client.broadcast.to(data[0].user).emit('delete', data[0])
      })
    })

  } catch (e) {
    console.log("Server Error ", e.message)
    process.exit(1)
  }
}

start()

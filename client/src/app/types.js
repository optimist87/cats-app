// @flow

export type CatItem = {
  _id?: string,
  breeds?: Array<any>,
  categories?: Array<any>,
  height: number,
  id: string,
  url: string,
  width: number,
  owner?: string,
}

export type StatusAction = {
  type: string,
  payload: boolean,
}

export type PropsForRequest = {
  url: string,
  method: string,
  body?: ?Object,
  headers?: Object,
}

export type FavoritesContextType = {
  favProcess: boolean,
  favoritesList: CatItem[],
  getFavorites: () => ?Promise<any>,
  addToFavorites: (CatItem: CatItem) => ?Promise<any>,
  deleteFromFavorites: (catIds: string[]) => ?Promise<any>,
  clearFavorites: () => ?void,
}

export type AuthContextType = {
  token: ?string,
  userId: ?string,
  login: (token: string, userId: string) => ?void,
  logout: () => ?void,
  isAuthenticated: boolean,
}

export type OnlineContextType = {
  isOnline: boolean,
}

export type AddSocketType = {
  favoritesList: CatItem[],
  cat: CatItem,
}

export type DeleteSocketType = {
  favoritesList: CatItem[],
  catIds: string[],
}


import React from "react"
import { shallow } from "enzyme"
import { Main } from "../Main"

describe("<Main />", () => {
  it("snapshot", () => {
    const component = shallow(<Main />)
    expect(component).toMatchSnapshot()
  });
})

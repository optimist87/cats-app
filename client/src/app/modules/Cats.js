// @flow

import React, { Fragment, useState, useEffect, useRef, useContext } from "react";
import { FavoritesContext } from "~/app/context/FavoritesContext"
import { OnlineContext } from "~/app/context/OnlineContext"
import { AuthContext } from "~/app/context/AuthContext"
import Loader from "~/app/components/Loader"
import Button from "~/app/components/Button"
import api from "~/app/api"

export function Cats () {

  const { favProcess, favoritesList, addToFavorites, deleteFromFavorites } = useContext(FavoritesContext)
  const { isOnline } = useContext(OnlineContext)
  const { isAuthenticated } = useContext(AuthContext)

  const [catItem, setCatItem] = useState([])
  const [allBreeds, setAllBreeds] = useState([])
  const [loading, setLoading] = useState(false)
  const [selectedBreed, setSelectedBreed] = useState("")

  const mounted = useRef(false)

  useEffect(() => {
    mounted.current = true
    if (isOnline) {
      getBreeds()
    }
    return () => {
      mounted.current = false
    }
  }, [isOnline])

  const getCats = async (breed?: string) => {
    setLoading(true)
    await api.getCats(breed)
      .then(response => {
        if (mounted.current) {
          setCatItem(response)
          setLoading(false)
        }
      })
      .catch(() => {
        setLoading(false)
      })
  }

  const getBreeds = async () => {
    setLoading(true)
    await api.getBreeds()
      .then(response => {
        if (mounted.current) {
          setAllBreeds(response)
          getInitialBreed(response)
          setLoading(false)
        }
      })
      .catch(() => {
        setLoading(false)
      })
  }

  const getInitialBreed = response => {
    if (!response.length) return
    const breed = response[0].id
    getCats(selectedBreed || breed)
    !selectedBreed && setSelectedBreed(breed)
  }

  const selectBreed = (event: any) => {
    const select: HTMLSelectElement = event.target
    setSelectedBreed(select.value);
    getCats(select.value)
  }

  return (
    <section>
      <h1>Котики</h1>
      <div className="cats__select">
        <select
          value={selectedBreed}
          onChange={selectBreed}
          disabled={!isOnline}
        >
          {allBreeds && allBreeds.map(breed => {
            return (<option key={breed.id} value={breed.id}>{breed.name}</option>)
          })}
        </select>
      </div>
      {loading ? (
        <Loader />
      ) : (
        <Fragment>
          <div className="cats__container">
            <div className="cats__img">
              {catItem && catItem.map(cat => {
                const isFavoriteImg = favoritesList.length && favoritesList.some(item => {return item.id === cat.id})
                return (
                  <div className="cats__img__inner" key={cat.id}>
                    <img src={cat.url} alt="" />
                    {isAuthenticated &&
                      (!isFavoriteImg ? (
                        <Button
                          onClick={() => addToFavorites(cat)}
                          disabled={favProcess}
                        >
                          В избранное
                        </Button>
                      ) : (
                        <Button
                          onClick={() => deleteFromFavorites([cat.id])}
                          disabled={favProcess}
                        >
                          Удалить из избранного
                        </Button>)
                      )
                    }
                  </div>
                )
              })}
            </div>
          </div>
          <div className="cats__buttons">
            <Button
              onClick={() => getCats(selectedBreed)}
            >
              Еще котиков
            </Button>
          </div>
        </Fragment>
      )}
    </section>
  )
}

export default Cats

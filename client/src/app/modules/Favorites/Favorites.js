// @flow

import React, { useState, useContext } from "react"
import { Redirect } from "react-router-dom"
import classNames from "classnames"
import { FavoritesContext } from "~/app/context/FavoritesContext"
import { AuthContext } from "~/app/context/AuthContext"
import Button from "~/app/components/Button"

import type { CatItem } from "~/app/types"

export function Favorites () {

  const { favProcess, favoritesList, deleteFromFavorites } = useContext(FavoritesContext)
  const { isAuthenticated } = useContext(AuthContext)
  const [marked, setMarked] = useState([])


  const handleMarked = (id: string) => {
    setMarked([...marked, id])
  }

  const handleUnMarked = (id: string) => {
    setMarked(marked.filter(i => i !== id))
  }

  const handleClearMarked = () => {
    deleteFromFavorites(marked)
    setMarked([])
  }

  if (!isAuthenticated) return <Redirect to="/"/>

  return (
    <section>
      <h1>Избранное</h1>
      <div className="fav__container">
        {!!favoritesList.length && favoritesList.map(cat => {
          const isMarkToDelete = marked.includes(cat.id)
          return (
            <div
              className={classNames(
                "fav__item",
                isMarkToDelete && "fav__item--marked"
              )}
              key={cat.id}
            >
              <img src={cat.url} />
              {!isMarkToDelete ? (
              <Button
                onClick={() => handleMarked(cat.id)}
                disadled={favProcess}
              >
                Удалить
              </Button>
              ) : (
              <Button
                className="btn btn--yellow"
                onClick={() => handleUnMarked(cat.id)}
                disadled={favProcess}
              >
                Вернуть
              </Button>
              )}
            </div>
          )
        })}
      </div>
      <div className="centered">
        {!!marked.length && (
          <Button
            onClick={handleClearMarked}
            disadled={favProcess}
          >
            Удалить отмеченные
          </Button>
        )}
      </div>
    </section>
  )
}

export default Favorites

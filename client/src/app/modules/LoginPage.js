// @flow

import React, { Fragment } from "react";

import LoginForm from "~/app/components/auth/LoginForm"

export function LoginPage () {

  return (
    <Fragment>
      <h1>Вход</h1>
      <LoginForm />
    </Fragment>
  )

}

export default LoginPage

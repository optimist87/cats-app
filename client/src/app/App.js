// @flow

import React, { Fragment, useState, useEffect, useContext } from "react"
import { Route, Switch } from "react-router-dom"
import { AuthContext } from "~/app/context/AuthContext"
import { FavoritesContext } from "~/app/context/FavoritesContext"
import { OnlineContext } from "~/app/context/OnlineContext"
import { useAuth } from "~/app/hooks"
import Header from "~/app/components/Header"
import OfflineNotification from "~/app/components/OfflineNotification"
import Loader from "~/app/components/Loader"
import Main from "~/app/modules/Main"
import Cats from "~/app/modules/Cats"
import Favorites from "~/app/modules/Favorites"
import LoginPage from "~/app/modules/LoginPage"
import RegisterPage from "~/app/modules/RegisterPage"
import api from "~/app/api";
import * as Socket from "~/app/sockets"

export function App () {

  const { getFavorites } = useContext(FavoritesContext)
  const { isOnline } = useContext(OnlineContext)
  const { ready, userId, isAuthenticated } = useAuth()

  useEffect(() => {
    if (isAuthenticated && isOnline && ready) {
      Socket.login(userId)
      getFavorites()
    }
  }, [isAuthenticated, isOnline, ready])

  if (!ready) return <Loader />

  return (
    <Fragment>
      <Header />
      <Switch>
        <Route exact path="/" component={Main} />
        <Route exact path="/image/" component={Cats} />
        <Route exact path="/favorites/" component={Favorites} />
        <Route exact path="/login/" component={LoginPage} />
        <Route exact path="/register/" component={RegisterPage} />
      </Switch>
      <OfflineNotification />
    </Fragment>
  )

}

export default App

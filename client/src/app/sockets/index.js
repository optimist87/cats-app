// @flow

import openSocket from "socket.io-client"
import { API_URL } from "~/app/config"

type Callback = (any) => void
type Data = string | Object

let apiUrl: string
if (process.env.NODE_ENV === "development") {
  apiUrl = API_URL.development
} else {
  apiUrl = API_URL.production
}
const socket = openSocket(apiUrl)

export const init = (type: string, callback: Callback) => {
  socket.on(type, callback)
}

export const send = (type: string, data: Data) => {
  socket.emit(type, data)
}

export const login = (userId: string) => {
  init("join", () => {})
  send("join", userId)
}

// @flow

import React from "react"
import { OnlineContext } from "~/app/context/OnlineContext"
import { useMakeOnline } from "~/app/hooks"


type Props = {
  children: any,
}

export const OnlineProvider = ({ children }: Props) => {
  const online = useMakeOnline()

  return (
    <OnlineContext.Provider
      value={{
        isOnline: online,
      }}
    >
      {children}
    </OnlineContext.Provider>
  )

}

export default OnlineProvider

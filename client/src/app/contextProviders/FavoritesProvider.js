// @flow

import React, { useState, useEffect } from "react"
import { FavoritesContext } from "~/app/context/FavoritesContext"
import { useMakeOnline, useAuth } from "~/app/hooks"
import api from "~/app/api"
import * as Socket from "~/app/sockets"

import type { CatItem, AddSocketType, DeleteSocketType } from "~/app/types";

type Props = {
  children: any,
}

export const FavoritesProvider = ({ children }: Props) => {
  const [favProcess, setFavProcess] = useState(false)
  const [favoritesList, setFavoritesList] = useState([])
  const { logout, token, userId, ready, isAuthenticated } = useAuth()
  const online = useMakeOnline()

  useEffect(() => {
    if (ready && isAuthenticated) {
      Socket.init("add", (data: AddSocketType) => addToFavoritesSocket(data))
      Socket.init("delete", (data: DeleteSocketType) => deleteFromFavoritesSocket(data))
    }
  }, [ready, isAuthenticated])

  const handleCloseSession = (error) => {
    if (error.message === "jwt expired") {
      logout()
      alert("Сессия истекла, Вам необходимо войти заново")
      clearFavorites()
      window.location.replace("/")
    }
  }

  const getFavorites = async () => {
    if (!token || !online) return
    setFavProcess(true)
    await api.getFavorites()
      .then(response =>  {
        if (response.success) {
          setFavoritesList(response.favoritesList)
          setFavProcess(false)
        } else throw new Error(response.message)
      })
      .catch((e) => {
        setFavProcess(false)
        handleCloseSession(e)
      })
  }

  const addToFavorites = async (cat: CatItem) => {
    if (!isAuthenticated || !online) return
    setFavProcess(true)
    await api.addToFavorites(cat)
      .then(response =>  {
        if (response.success) {
          setFavoritesList(response.favoritesList)
          Socket.send("add", { favoritesList, cat, user: userId })
          setFavProcess(false)
        } else throw new Error(response.message)
      })
      .catch((e) => {
        setFavProcess(false)
        handleCloseSession(e)
      })
  }

  const deleteFromFavorites = async (catIds: string[]) => {
    if (!isAuthenticated || !online) return
    setFavProcess(true)
    await api.deleteFromFavorites(catIds)
      .then(response =>  {
        if (response.success) {
          setFavoritesList(response.favoritesList)
          Socket.send("delete", { favoritesList, catIds, user: userId })
          setFavProcess(false)
        } else throw new Error(response.message)
      })
      .catch((e) => {
        setFavProcess(false)
        handleCloseSession(e)
      })
  }

  const clearFavorites = () => {
    setFavoritesList([])
  }

  const addToFavoritesSocket = ({ favoritesList, cat }) => {
    setFavoritesList([cat, ...favoritesList])
  }

  const deleteFromFavoritesSocket = ({ favoritesList, catIds }) => {
    const updatedFavoritesList = favoritesList.filter(item => {
      return !catIds.includes(item.id)
    })
    setFavoritesList(updatedFavoritesList)
  }

  return (
    <FavoritesContext.Provider
      value={{
        favProcess,
        favoritesList,
        getFavorites,
        addToFavorites,
        deleteFromFavorites,
        clearFavorites,
      }}
    >
      {children}
    </FavoritesContext.Provider>
  )

}

export default FavoritesProvider

// @flow

import React from "react"
import { AuthContext } from "~/app/context/AuthContext"
import { useAuth } from "~/app/hooks"


type Props = {
  children: any,
}

export const AuthProvider = ({ children }: Props) => {

  const { login, logout, token, userId, ready, isAuthenticated } = useAuth()

  return (
    <AuthContext.Provider
      value={{
        login,
        logout,
        token,
        userId,
        ready,
        isAuthenticated,
      }}
    >
      {children}
    </AuthContext.Provider>
  )

}

export default AuthProvider

// @flow

import React, { useState, useEffect, useContext } from "react";
import classNames from "classnames";
import { OnlineContext } from "~/app/context/OnlineContext"

export function OfflineNotification () {

  const { isOnline } = useContext(OnlineContext)
  const [show, setShow] = useState(false)

  useEffect(() => {
    setTimeout(() => setShow(!isOnline), 100);
  }, [isOnline])

  if (isOnline) return null
  return <div className={classNames("notification", show && "notification--show")}>Вы Оффлайн...</div>
}

export default OfflineNotification

// @flow

import React, { useContext } from "react";
import { OnlineContext } from "~/app/context/OnlineContext"

type Props = {
  onClick?: () => ?Promise<any>,
  children: string,
  className?: string,
  type?: string,
  disabled?: boolean,
}

export function Button ({
  onClick,
  children,
  className,
  type,
  disabled,
}: Props) {

  const { isOnline } = useContext(OnlineContext)

  return (
    <button
      className={className || "btn"}
      type={type || "button"}
      onClick={onClick}
      disabled={disabled || !isOnline}
    >
      {children}
    </button>
  )
}

export default Button

// @flow

import React, { useContext } from "react";
import { OnlineContext } from "~/app/context/OnlineContext"

export function Loader () {

  const { isOnline } = useContext(OnlineContext)

  return (
    <div className="loading">
      {isOnline ? "Загрузка..." : "Вы оффлайн..."}
    </div>
  )
}

export default Loader

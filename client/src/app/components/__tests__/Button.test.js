import React from "react"
import { shallow } from "enzyme"
import { Button } from "../Button"

describe("<Button />", () => {
  it("snapshot", () => {
    const component = shallow(<Button />)
    expect(component).toMatchSnapshot()
  })

  it("snapshot with full props", () => {
    const props = {
      children: "Кнопка",
      className: "button",
      type: "submit",
      disabled: false,
    }
    const component = shallow(<Button {...props} />)
    expect(component).toMatchSnapshot()
  })

  test("onClick", () => {
    const props = {
      children: "Кнопка",
      className: "button",
      type: "submit",
      disabled: false,
      onClick: jest.fn()
    }
    const component = shallow(<Button {...props} />)
    component.find("button").simulate("click")
    expect(props.onClick).toBeCalled()
  })
})

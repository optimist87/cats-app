// @flow

import React from "react";
import { useHistory } from "react-router-dom";
import { Formik } from "formik";
import Button from "~/app/components/Button"
import { REG_EXP_FOR_EMAIL } from "~/app/constants"
import api from "~/app/api";

import type { FormikProps } from "formik";

export function RegisterForm () {

  const history = useHistory()

  const handleSubmit = async (values, { setSubmitting, setErrors }) => {
    api.register(values)
      .then(response => {
        if (response.success) {
          alert(response.message)
          setSubmitting(false)
          history.push("/login")
        } else throw new Error(response.message)
      })
      .catch(errors => {
        setSubmitting(false)
        alert(errors.message)
        setErrors(errors.message)
      })
}

  return (
    <Formik
      initialValues={{ email: "", password: "" }}
      validate={values => {
        let errors = {}
        if (!values.email) {
          errors.email = "Заполните email"
        } else if (
          REG_EXP_FOR_EMAIL.test(values.email)
        ) {
          errors.email = "Неправильный email"
        }
        if (!values.password) {
          errors.password = "Пустой пароль"
        }
        return errors
      }}
      onSubmit={handleSubmit}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }: FormikProps) => (
        <form className="form" onSubmit={handleSubmit}>
          <div className="field">
            <input
              type="email"
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
            />
            <div className="error">
              {errors.email && touched.email && errors.email}
            </div>
          </div>
          <div className="field">
            <input
              type="password"
              name="password"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.password}
            />
            <div className="error">
              {errors.password && touched.password && errors.password}
            </div>
          </div>
          <Button
            type="submit"
            disabled={isSubmitting}
          >
            Submit
          </Button>
        </form>
        
      )}
    </Formik>
  )

}

export default RegisterForm

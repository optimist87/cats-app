// @flow

import React, { useContext } from "react";
import { useHistory } from "react-router-dom"
import { FavoritesContext } from "~/app/context/FavoritesContext"
import { AuthContext } from "~/app/context/AuthContext"
import Button from "~/app/components/Button"

function SignOutButton () {

  const { clearFavorites } = useContext(FavoritesContext)
  const { logout } = useContext(AuthContext)
  const history = useHistory()

  const signOut = () => {
    logout()
    alert("Вы вышли")
    clearFavorites()
    history.push("/")
  }

  return (
    <Button
      className="link"
      onClick={signOut}
    >
      Выход
    </Button>
  )
}

export default SignOutButton

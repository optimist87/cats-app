// @flow

import React, { Fragment, useContext } from "react";
import { NavLink } from "react-router-dom";
import { FavoritesContext } from "~/app/context/FavoritesContext"
import { AuthContext } from "~/app/context/AuthContext"
import SignOutButton from "./auth/SignOut";

export function Header () {

  const { favoritesList } = useContext(FavoritesContext)
  const { isAuthenticated } = useContext(AuthContext)
  const isShowFavorites = isAuthenticated && !!favoritesList.length
  return (
    <header className="header">
      <div className="header__inner">
        <nav>
          <ul>
            <li><NavLink to="/">Главная</NavLink></li>
            <li><NavLink to="/image">Котики</NavLink></li>
            {isShowFavorites && (<li><NavLink to="/favorites">Избранное</NavLink></li>)}
          </ul>
        </nav>
        <div className="auth">
          <ul>
            {isAuthenticated ?
              <SignOutButton />
              : (
                <Fragment>
                  <li><NavLink to="/login">Вход</NavLink></li>
                  <li><NavLink to="/register">Регистрация</NavLink></li>
                </Fragment>
              )
            }
          </ul>
        </div>
      </div>
    </header>
  )

}

export default Header

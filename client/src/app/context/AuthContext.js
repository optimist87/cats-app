// @flow

import { createContext } from "react"

import type { AuthContextType } from "~/app/types"

export const AuthContext = createContext<AuthContextType>({
  token: null,
  userId: null,
  login: () => null,
  logout: () => null,
  isAuthenticated: false,
})

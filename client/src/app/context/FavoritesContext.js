// @flow

import { createContext } from "react"

import type { FavoritesContextType } from "~/app/types"

export const FavoritesContext = createContext<FavoritesContextType>({
  favProcess: false,
  favoritesList: [],
  getFavorites: () => null,
  addToFavorites: () => null,
  deleteFromFavorites: () => null,
  clearFavorites: () => null
})

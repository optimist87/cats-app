// @flow

import { createContext } from "react"

import type { OnlineContextType } from "~/app/types"

export const OnlineContext = createContext<OnlineContextType>({
  isOnline: true,
})

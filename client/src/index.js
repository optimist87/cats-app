import React from "react"
import { render } from "react-dom"
import { BrowserRouter } from "react-router-dom"
import FavoritesProvider from "~/app/contextProviders/FavoritesProvider"
import OnlineProvider from "~/app/contextProviders/OnlineProvider"
import AuthProvider from "~/app/contextProviders/AuthProvider"
import App from "~/app/App"
import registerServiceWorker from './registerServiceWorker'
import "./style.scss"

const element = document.getElementById("root")

render(
  <OnlineProvider>
    <FavoritesProvider>
      <BrowserRouter>
        <AuthProvider>
          <App />
        </AuthProvider>
      </BrowserRouter>
    </FavoritesProvider>
  </OnlineProvider>,
  element
)
registerServiceWorker()

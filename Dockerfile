FROM node:10.20-alpine
WORKDIR /usr/src/app
COPY . .
RUN npm install
RUN npm run client:install
RUN npm run client:build
EXPOSE 8080
CMD ["npm","run", "dev"]
